import loc_uis
from PyQt5 import QtWidgets, uic
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

Ui_preview, _ = uic.loadUiType(loc_uis.get_path_to("preview.ui", current=__file__))


class Preview(QtWidgets.QMainWindow, Ui_preview):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_preview.__init__(self)
        self.setupUi(self)
        self.setParent(None)
        self.id = None

        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.canvas_layout.addWidget(self.toolbar)
        self.canvas_layout.addWidget(self.canvas)

        self.ax = self.figure.add_subplot(111)
        self.ax.axis([0, 1, 0, 1])
        self.ax.set_xlabel("kt")
        self.ax.set_ylabel("kn")
        self.curves = dict()
        self.ax.grid("on")
        self.plot_empty_data()
        self.plot_empty_classified_data()
        self.plot_empty_close_points()
        self.plot_empty_path()

    def set_id(self, _id):
        self.id = _id

    def get_id(self):
        return self.id

    def plot_empty_data(self):
        self.curves["data"] = self.ax.plot(
            [], [], ".b", alpha=0.9, label="Data"
            )

    def plot_empty_path(self):
        self.curves["path"] = self.ax.plot(
            [], [], "-k", alpha=0.9, label="Path"
            )

    def plot_empty_classified_data(self):
        self.curves["class_data"] = self.ax.plot(
            [], [], ".g", alpha=0.9, label=None
            )

    def plot_empty_close_points(self):
        self.curves["close_points"] = self.ax.plot(
            [], [], ".", color="orange", alpha=0.9, label=None
            )

    def update_data(self, new_data):
        handler = self.curves["data"][0]
        handler.set_xdata(new_data["kt"])
        handler.set_ydata(new_data["kn"])
        self.canvas.draw()

    def update_classified_data(self, new_data):
        handler = self.curves["class_data"][0]
        handler.set_xdata(new_data["kt"])
        handler.set_ydata(new_data["kn"])
        handler.set_label("Good Data")
        self.canvas.draw()

    def update_close_points(self, new_data):
        handler = self.curves["close_points"][0]
        handler.set_xdata(new_data["kt"])
        handler.set_ydata(new_data["kn"])
        handler.set_label("Close points")
        self.ax.legend(loc=2)
        self.canvas.draw()

    def update_path(self, new_path):
        handler = self.curves["path"][0]
        handler.set_xdata(new_path.vertices[:, 0])
        handler.set_ydata(new_path.vertices[:, 1])
        self.ax.legend(loc=2)
        self.canvas.draw()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    windows = Preview()
    app.exec_()
