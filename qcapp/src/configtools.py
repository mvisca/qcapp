import configparser
import os
import loc_uis

CONFIG_FILE = loc_uis.get_path_cfg("config.cfg", current=__file__)


class ConfigFile:
    def __init__(self):
        self.cfg_handler = configparser.ConfigParser()
        if os.path.exists(CONFIG_FILE):
            self.cfg_handler.read(CONFIG_FILE)
        self.filename = CONFIG_FILE
        self.mode = "w"

    def __enter__(self):
        self.file_handler = open(self.filename, "w")
        return self.file_handler

    def __exit__(self, *args):
        self.cfg_handler.write(self.file_handler)
        self.file_handler.close()

    def add_section(self, section, content):
        self.cfg_handler.add_section(section)
        for key, value in content.items():
            self.cfg_handler.set(section, key, value)

    def update_section(self, section, new_content):
        for option, value in new_content.items():
            self.cfg_handler.set(section, option, value)

    def print(self):
        for sec in self.cfg_handler.sections():
            print(f"[{sec}]")
            for opt, val in self.cfg_handler.items(sec):
                print(f"{opt} = {val}")
            print("")


if __name__ == '__main__':
    cfg_file = ConfigFile()

    _ZONE1 = {
        "upper_a": "",
        "upper_b": "",
        "upper_c": "",
        "mid_a": "",
        "mid_b": "",
        "mid_c": "",
        "lower_a": "",
        "lower_b": "",
        "lower_c": "",
        "upper_max_iters": "10000",
        "mid_max_iters": "10000",
        "lower_max_iters": "10000",
        "tol": "1e-5",
        "bins": "100",
        "k_factor": "2"
    }
    _ZONE2 = {
        "upper_a": "",
        "upper_b": "",
        "upper_c": "",
        "mid_a": "",
        "mid_b": "",
        "mid_c": "",
        "lower_a": "",
        "lower_b": "",
        "lower_c": "",
        "upper_max_iters": "10000",
        "mid_max_iters": "10000",
        "lower_max_iters": "10000",
        "tol": "1e-5",
        "bins": "100",
        "k_factor": "2"
    }
    _ZONE3 = {
        "upper_a": "",
        "upper_b": "",
        "upper_c": "",
        "mid_a": "",
        "mid_b": "",
        "mid_c": "",
        "lower_a": "",
        "lower_b": "",
        "lower_c": "",
        "upper_max_iters": "10000",
        "mid_max_iters": "10000",
        "lower_max_iters": "10000",
        "tol": "1e-5",
        "bins": "100",
        "k_factor": "2"
    }
    with cfg_file:
        cfg_file.add_section("ZONE1", _ZONE1)
        cfg_file.add_section("ZONE2", _ZONE2)
        cfg_file.add_section("ZONE3", _ZONE3)
        cfg_file.add_section("METADATA", {})
