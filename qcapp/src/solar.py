"""
Libreria de funciones solares.
Traduccion de solar_04.jl a python.
"""
import calendar
import numpy as np

SC = 1367.0  # solar constant
Ic = 4.9212  # hourly solar constant, MJ/m2 = 1367 Wh/m2
Hc = 37.5952 # daily solar constant, MJ/m2 = Ic x 24/pi Wh/m2

def declination(n, y):
    """ Declinación solar """
    days = 366 if calendar.isleap(y) else 365
    gamma = 2*np.pi*(n-1)/days

    d = 0.006918 - 0.399912*np.cos(gamma) + 0.070257*np.sin(gamma) - 0.006758*np.cos(2*gamma) + 0.000907*np.sin(2*gamma) - 0.002697*np.cos(3*gamma) + 0.00148*np.sin(3*gamma)

    return d

def WS(d, lat):
    """ Sunset hour angle for horizontal plane """

    ws = np.arccos(-np.tan(d)*np.tan(lat))

    return ws

def EoT(n, y=2001):
    """ Equation of time, minutes """
    days = 366 if calendar.isleap(y) else 365

    gam = 2*np.pi*(n-1)/days;
    eot=60*(3.8196667*(0.0000075+0.001868*np.cos(gam)- \
        0.032077*np.sin(gam)-0.014615*np.cos(2*gam)-0.04089*np.sin(2*gam)))

    return eot


def WH(st, lon, eot, utc):
    """ Hour angle, rads """
    lon0 = np.pi*utc/12.0 # longitud uso horario
    w = np.pi*(st+eot/60)/12 - np.pi + lon-lon0

    return w


def Hoh(fn, d, ws, lat):
    """ Irradiación solar diaria """

    H = Hc*fn*(np.cos(d)*np.cos(lat)*np.sin(ws) + ws*np.sin(d)*np.sin(lat))
    return H

def Fn(n, y=2001):
    """ Factor de distancia solar """
    days = 366 if calendar.isleap(y) else 365
    gam = 2*np.pi*(n - 1)/days
    fn = 1.000110+0.034221*np.cos(gam) + 0.001280*np.sin(gam) + 0.000719*np.cos(2*gam)+0.000077*np.sin(2*gam)

    return fn


def CZ(w, d, lat):

    ctz = np.sin(d)*np.sin(lat)+np.cos(d)*np.cos(lat)*np.cos(w)
    return ctz


def CZe(st, lat, lon, n, y, utc=-3):
    d = declination(n,y)               # declination
    eot = EoT(n,y)             # Eq. of time
    w = WH(st,lon,eot,utc)        # hour angle
    ctz = CZ(w,d,lat)          # Cos z
    return ctz


def relative_optic_mass(cz, z):
    """ Masa optica relativa """
    pcoef = np.exp(-z/8435.2)
    cenit = np.rad2deg(np.arccos(cz))
    gama = 90 - cenit
    ret = np.zeros(shape=cz.shape)
    msk = gama >= 0
    div = (np.sin(np.deg2rad(gama[msk])) + 0.50572*(gama[msk]+6.07995)**(-1.6364))
    ret[msk] = pcoef/div
    if ret.size == 1:
        return np.asscalar(ret)
    else:
        return ret


def Kt(ghi, mass, fn=1.0):
    """ Clearness Index Kt"""
    Go = SC*fn

    return ghi*mass/Go


def Kt_perez(kt, mass):
    """ Perez's clearness Index """
    val = kt/(1.031*np.exp(-1.4/(0.9 + 9.4*(1/mass))) + 0.1)

    return val

def Kt_diario(ghi, n, year, lat):
    """ kt diario """
    fn = Fn(n, year)
    d = declination(n, year)
    ws = WS(d, lat)
    Go = Hoh(fn, d, ws, lat)

    return ghi/Go

def decimal_hour(n=1):
    """ Retorna el vector hora decimal para n dias"""
    hours = np.arange(start=0, stop=24, step=1) # rango entre 0 y 23 horas
    minutes= np.arange(start=0, stop=60, step=1) # rango entre 0 y 59 minutos

    vec = np.repeat(hours, 60) + (np.tile(minutes, 24))/60

    return np.tile(vec, n)
