import numpy as np
from matplotlib.path import Path

import gompertz as gompertz
import configtools
import time
import numba


class GetEnvelopes(object):
    def __init__(self, _id=None):
        # Creates the curves
        self.x_coords = None
        self.id = _id
        self.sup_curve = None
        self.mid_curve = None
        self.inf_curve = None
        self.status = dict()

    def construct(self):
        self.SETTINGS = configtools.ConfigFile().cfg_handler
        self.sup_curve = gompertz.Gompertz(self.get_seeds("upper"))
        self.mid_curve = gompertz.Gompertz(self.get_seeds("mid"))
        self.inf_curve = gompertz.Gompertz(self.get_seeds("lower"))
        self.set_settings()

    def get_seeds(self, curve):
        a = self.SETTINGS.getfloat(f"ZONE{self.id}", f"{curve}_a")
        b = self.SETTINGS.getfloat(f"ZONE{self.id}", f"{curve}_b")
        c = self.SETTINGS.getfloat(f"ZONE{self.id}", f"{curve}_c")
        return [a, b, c]

    def set_settings(self):
        self.settings = {
            "upper_max_iters": self.SETTINGS.getint(f"ZONE{self.id}", "upper_max_iters"),
            "mid_max_iters": self.SETTINGS.getint(f"ZONE{self.id}", "mid_max_iters"),
            "lower_max_iters": self.SETTINGS.getint(f"ZONE{self.id}", "lower_max_iters"),
            "tol": self.SETTINGS.getfloat(f"ZONE{self.id}", "tol"),
            "bins": self.SETTINGS.getint(f"ZONE{self.id}", "bins"),
            "k_factor": self.SETTINGS.getfloat(f"ZONE{self.id}", "k_factor"),
        }

    @numba.jit(nogil=True)
    def get_envelopes(self, data=None):
        k = self.settings["k_factor"]
        bins = self.settings["bins"]
        tol = self.settings["tol"]

        df = data.sort_values(by="kt", ascending=1, inplace=False)
        # Data of interest
        msk = (df["kt"].between(0, 1)) & (df["kn"].between(0, 1))
        df.drop(df.index[~msk], inplace=True)  # Delete the garbage

        # Fit de mid_curve
        self.mid_curve.fit(
            xdata=df["kt"].values, ydata=df["kn"].values,
            max_iters=self.settings["mid_max_iters"],
            tol=tol,
            )
        y_values = self.mid_curve.function(df["kt"])
        std_error = rmsd(df["kn"], y_values)
        # New data of interest
        msk_std = (df["kn"] <= y_values + k*std_error) & \
            (df["kn"] >= y_values - k*std_error)
        # Delete new garbage
        df.drop(df.index[~msk_std], inplace=True)
        y_values = y_values[msk_std]
        # Re-calc std error
        std_error = rmsd(df["kn"], y_values)
        # Data for the sup. and the inf. envelope.
        y_sup = np.copy(y_values)
        y_inf = np.copy(y_values)
        # Make the intervals
        intervals = zip(
            np.linspace(df["kt"].min(), df["kt"].max(), bins)[:-1],
            np.linspace(df["kt"].min(), df["kt"].max(), bins)[1:],
        )
        # Calculate the value of rmsd for each interval
        # Generate the points for the envelopes
        for lower, upper in intervals:
            msk_bin = np.logical_and(df["kt"] >= lower, df["kt"] < upper)
            if np.sum(msk_bin) != 0:
                std_error_bin = rmsd(df["kn"][msk_bin], y_values[msk_bin])
            else:
                std_error_bin = 0

            y_sup[msk_bin] += k*std_error_bin
            y_inf[msk_bin] -= k*std_error_bin
        # Fit de sup_curve
        self.sup_curve.fit(
            xdata=df["kt"].values, ydata=y_sup,
            max_iters=self.settings["upper_max_iters"],
            tol=tol,
            )
        # Fit de inf_curve
        self.inf_curve.fit(
            xdata=df["kt"].values, ydata=y_inf,
            max_iters=self.settings["lower_max_iters"],
            tol=tol,
            )
        self.x_coords = df["kt"]
        
        self.status["mid"] = self.mid_curve.status
        self.status["upper"] = self.sup_curve.status
        self.status["lower"] = self.inf_curve.status

    def make_path(self):
        sup_curve = self.sup_curve.function(self.x_coords)
        inf_curve = self.inf_curve.function(self.x_coords)
        inf_curve = inf_curve - inf_curve.min()  # Correction, must touch 0
        # Find points where inf_curve > sup_curve, when kt->1
        msk = (self.x_coords > 0.6) & (inf_curve > sup_curve)
        sup_curve = sup_curve[~msk]
        inf_curve = inf_curve[~msk]
        # Make path
        verts = [(x, y) for x, y in zip(self.x_coords, sup_curve)]
        verts_inf = [(x, y) for x, y in zip(self.x_coords, inf_curve)]
        verts.extend(reversed(verts_inf))
        codes = [Path.MOVETO]
        codes.extend([Path.LINETO for i in range(len(verts) - 2)])
        codes.append(Path.CLOSEPOLY)
        self.path = Path(verts, codes)
        return self.path

    def points_inside(self, data):
        points = zip(data["kt"], data["kn"])
        ret = self.path.contains_points(list(points))
        return ret

    def points_near_path(self, data, distance=0.035):
        ret = np.fromiter((is_ok2(
            x,
            self.path,
            self.sup_curve.function,
            self.inf_curve.function,
            distance
            ) for x in data), bool, len(data))

        return ret


def rmsd(data, ref):
    return np.sqrt(np.mean(np.square(data - ref)))


def dist(point, ref):
    return np.sqrt(np.sum((point-ref)**2, axis=1))


def gpoint(x, f):
    return np.array([x, f(x)])

def is_ok2(point, path, sup, inf, d):
    if np.isnan(point).any():
        return False
    else:
        i = np.linspace(point[0]-0.1, point[0]+0.1, 50)
        d1 = dist(gpoint(i, sup).T, point)
        d2 = dist(gpoint(i, inf).T, point)

        d1 = np.min(d1)
        d2 = np.min(d2)
        if np.min((d1, d2)) < d:
            return True
        else:
            return False
