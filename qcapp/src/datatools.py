import pandas as pd
import numpy as np
import os
import collections
import configtools
import solar


seasons = {
    range(1, 80): 'summer',
    range(80, 172): 'autumn',
    range(172, 264): 'winter',
    range(264, 355): 'spring',
    range(355, 366): 'summer'
    }


class Data(object):
    def __init__(self, path):
        self.df = None
        self.settings = configtools.ConfigFile().cfg_handler
        self.read_data(path)
        self.do_solar_math()
        self.classify_by_zone()
        self.df["inside"] = False
        self.df["near"] = False

    def read_data(self, path):
        """ Read all the PX's in the path """
        files = sorted(os.listdir(path))
        frames = [pd.read_csv(path + file) for file in files]
        df = pd.concat(frames).reset_index(drop=True)
        df["Fecha"] = pd.to_datetime(df["Fecha"])
        self.df = df

    def do_solar_math(self):
        """ Make all the math stuff (kt, kn, kd, fn, etc) in self.df"""
        year = self.settings.getint("METADATA", "year")
        altitude = self.settings.getfloat("METADATA", "altitude")
        self.df["air_mass"] = solar.relative_optic_mass(
            self.df["CZ"].values, altitude
            )

        self.df["fn"] = solar.Fn(self.df["N"].values, year)

        for col in ["GHI", "DHI", "DNI"]:
            msk = self.df[col] < 0
            self.df.loc[msk, col] = np.nan

        Io = solar.SC*self.df["fn"].values
        self.df["kt"] = self.df["GHI"].values / (Io * self.df["CZ"].values)
        # self.df["kd"] = self.df["DHI"].values / (Io * self.df["CZ"].values)
        self.df["kn"] = self.df["DNI"].values / Io

        msk = self.df["CZ"] >= 0.0
        self.df.loc[~msk, "kt"] = 0
        self.df.loc[~msk, "kn"] = 0
        # self.df.loc[~msk, "kd"] = 0

    def classify_by_zone(self):
        self.df["zones"] = 0
        eps = np.finfo(np.float64).eps
        # First zone
        msk = self.df["air_mass"].between(1.0, 1.25)
        self.df.loc[msk, "zones"] = 1
        # Second zone
        msk = self.df["air_mass"].between(1.25+eps, 2.50)
        self.df.loc[msk, "zones"] = 2
        # Third zone
        msk = self.df["air_mass"].between(2.50+eps, 8.33)
        self.df.loc[msk, "zones"] = 3
        del msk

    def classify_by_season(self, doys):
        """ Creates a new column with seasons of year in self.df """
        self.df["seasons"] = map(lambda n: self.get_season(n), doys)

    def get_season(self, n):
        """ Take the day of year and return the respective season """
        for key in seasons.keys():
            if n in key:
                return seasons[key]

    def export_files_by_doy(self, path=None, prefix=None, progress=None):
        doys = np.unique(self.df["N"])
        filename = "/"+prefix+"_{}.csv"
        progress.setMaximum(len(doys))
        for i, doy in enumerate(doys):
            msk = self.df["N"] == doy
            output_df = pd.DataFrame(
                columns=["NR", "Fecha", "N", "T", "CZ", "F1", "F2"]
            )
            output_df["NR"] = self.df["NR"][msk]
            output_df["Fecha"] = self.df["Fecha"][msk]
            output_df["T"] = self.df["T"][msk]
            output_df["N"] = self.df["N"][msk]
            output_df["CZ"] = self.df["CZ"][msk]
            output_df["F1"] = self.df["inside"][msk]
            output_df["F2"] = self.df["near"][msk]

            str_doy = str(doy)
            if doy < 10:
                str_doy = "00"+str_doy
            elif doy < 100:
                str_doy = "0"+str_doy

            output_df.to_csv(path+filename.format(doy), index=False)
            del output_df
            progress.setValue(i+1)

    def make_stats(self):
        ret = collections.defaultdict(list)
        gral_nvalid = 0
        gral_ntagged = 0
        for zone in range(1, 4):
            msk = self.df["zones"] == zone
            msk2 = np.isnan(self.df["kt"][msk]) | np.isnan(self.df["kn"][msk])
            msk3 = self.df["inside"][msk] | self.df["near"][msk]
            nvalid = np.sum(~msk2)
            ntagged = np.sum(msk3)
            gral_nvalid += nvalid
            gral_ntagged += ntagged
            porcentage = 100*ntagged/nvalid
            ret[zone].append(nvalid)
            ret[zone].append(ntagged)
            ret[zone].append(100 - porcentage)
        gral_porcentage = 100*gral_ntagged/gral_nvalid
        ret[4].append(gral_nvalid)
        ret[4].append(gral_ntagged)
        ret[4].append(100 - gral_porcentage)
        return ret
