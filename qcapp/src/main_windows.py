import gc
import sys
import time
import warnings

from PyQt5 import QtWidgets, uic
import set_parameters
import seeds_selector
import stats_preview
import envelopes
import preview
import metadata
import datatools
import loc_uis

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
warnings.filterwarnings("ignore", module="matplotlib")

UI_main, _ = uic.loadUiType(loc_uis.get_path_to("MainWindows.ui", current=__file__))

class MainWindows(QtWidgets.QMainWindow, UI_main):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        UI_main.__init__(self)
        self.setupUi(self)
        self.setParent(None)

        self.progressBar.setEnabled(False)

        self.data = None
        self.load_data_btn.clicked.connect(self.load_data)

        self.z1_env = envelopes.GetEnvelopes(_id=1)
        self.z2_env = envelopes.GetEnvelopes(_id=2)
        self.z3_env = envelopes.GetEnvelopes(_id=3)

        self.btn_fit1.clicked.connect(lambda: self.fit_envs(_id=1))
        self.btn_fit2.clicked.connect(lambda: self.fit_envs(_id=2))
        self.btn_fit3.clicked.connect(lambda: self.fit_envs(_id=3))

        self.open_folder.clicked.connect(lambda: self.select_dir("in"))
        self.output_folder.clicked.connect(lambda: self.select_dir("out"))

        self.seeds_windows = seeds_selector.SeedsSelector()
        self.param_windows = set_parameters.ParametersWindows()
        self.preview_windows = preview.Preview()
        self.metadata_windows = metadata.MetadataWindows()
        self.stats_preview_windows = stats_preview.Preview()

        self.select_seeds1.clicked.connect(lambda: self.seeds_windows_show(1))
        self.select_seeds2.clicked.connect(lambda: self.seeds_windows_show(2))
        self.select_seeds3.clicked.connect(lambda: self.seeds_windows_show(3))

        self.set_param1.clicked.connect(lambda: self.param_windows_show(1))
        self.set_param2.clicked.connect(lambda: self.param_windows_show(2))
        self.set_param3.clicked.connect(lambda: self.param_windows_show(3))

        self.preview1.clicked.connect(lambda: self.preview_windows_show(1))
        self.preview2.clicked.connect(lambda: self.preview_windows_show(2))
        self.preview3.clicked.connect(lambda: self.preview_windows_show(3))

        self.metadata_btn.clicked.connect(self.metadata_windows_show)
        self.show_stats.clicked.connect(self.show_stats_windows)

        self.preview_windows.classify.clicked.connect(self.close_points)

        self.export_btn.clicked.connect(self.export)
        self.show()

    def select_dir(self, line):
        selector = {
            "in": self.path_LineEdit,
            "out": self.output_path,
        }
        obj = QtWidgets.QFileDialog.DontUseNativeDialog
        path = QtWidgets.QFileDialog.getExistingDirectory(
            parent=self, caption="Select Directory", options=obj
        )
        selector[line].setText(path)

    def export(self):
        self.progressBar.setEnabled(True)
        self.data.export_files_by_doy(
            path=self.output_path.text(),
            prefix=self.prefix.text(),
            progress=self.progressBar
        )
        QtWidgets.QMessageBox.information(self, "Message", "Done.")

    def load_data(self):
        path = self.path_LineEdit.text()
        self.data = datatools.Data(path+"/")
        QtWidgets.QMessageBox.information(self, "Message", "Done.")

    def param_windows_show(self, _id):
        self.param_windows.set_id(_id)
        self.param_windows.show()

    def metadata_windows_show(self):
        self.metadata_windows.show()
        self.load_data_btn.setEnabled(True)

    def seeds_windows_show(self, _id=None):
        self.seeds_windows.set_id(_id)
        try:
            msk = self.data.df["zones"] == _id
            self.seeds_windows.update_data(
                new_data=self.data.df.loc[msk, ["kn", "kt"]]
            )
        except AttributeError:
            pass
            # QtWidgets.QMessageBox.information(
            #     self,
            #     "Message",
            #     "You must process the data first."
            # )
            # return

        self.seeds_windows.show()

    def preview_windows_show(self, _id=None):
        selector = {
            1: self.z1_env,
            2: self.z2_env,
            3: self.z3_env,
        }
        try:
            path = selector[_id].path
        except AttributeError:
            QtWidgets.QMessageBox.information(
                self,
                "Message",
                "You must fit the envelopes first."
            )
            return

        self.preview_windows.set_id(_id)
        msk = self.data.df["zones"] == _id
        msk_ok = self.data.df["inside"][msk]
        self.preview_windows.update_data(
            new_data=self.data.df.loc[msk, ["kn", "kt"]]
            )
        self.preview_windows.update_classified_data(
            new_data=self.data.df.loc[msk, ["kn", "kt"]][msk_ok],
            )
        self.preview_windows.update_path(path)
        self.preview_windows.update_close_points(
            self.data.df.loc[(msk & self.data.df["near"]), ["kt", "kn"]]
        )
        self.preview_windows.show()

    def close_points(self):
        selector = {
            1: self.z1_env,
            2: self.z2_env,
            3: self.z3_env,
        }

        msk = (self.data.df["zones"] == self.preview_windows.id) & \
            ~(self.data.df["inside"])
        env = selector[self.preview_windows.id]
        self.data.df.loc[msk, "near"] = env.points_near_path(
            self.data.df.loc[msk, ["kt", "kn"]].values,
            self.preview_windows.distance.value()
        )
        self.preview_windows.update_close_points(
            self.data.df.loc[(msk & self.data.df["near"]), ["kt", "kn"]]
        )

    def classified_data(self, _id=None):
        selector = {
            1: self.z1_env,
            2: self.z2_env,
            3: self.z3_env,
        }
        env = selector[_id]
        msk = self.data.df["zones"] == _id
        self.data.df.loc[msk, "ok"] = env.classify(
            self.data.df.loc[msk, ["kt", "kn"]]
            )

    def fit_envs(self, _id=None):
        selector = {
            1: self.z1_env,
            2: self.z2_env,
            3: self.z3_env,
        }
        env = selector[_id]
        env.construct()
        try:
            msk = self.data.df["zones"] == _id
            env.get_envelopes(data=self.data.df.loc[msk, ["kn", "kt"]])
        except AttributeError:
            QtWidgets.QMessageBox.information(
                self,
                "Message",
                "You must process the data first."
            )
            return

        env.make_path()
        status = \
        """
        Status of the envelopes fit process:
        ---------------------------------
        Mid curve:
            converged: {} , iterations: {}
        Upper curve:
            converged: {} , iterations: {}
        Lower curve:
            converged: {} , iterations: {}
        """
        status_args = list()
        for key in env.status.keys():
            status_args.append(env.status[key].get("converged", None))
            status_args.append(env.status[key].get("iters_done", None))

        
        msg = QtWidgets.QMessageBox()
        msg.setText("Process done, plese ckeck the details.")
        msg.setIcon(QtWidgets.QMessageBox.Information)
        msg.setWindowTitle("Message")
        msg.setDetailedText(status.format(*status_args))
        msg.exec_()
        
        self.data.df.loc[msk, "inside"] = env.points_inside(
            self.data.df.loc[msk, ["kt", "kn"]]
        )

    def show_stats_windows(self):
        try:
            stats = self.data.make_stats()
        except AttributeError:
            QtWidgets.QMessageBox.information(
                self,
                "Message",
                "You must process the data first."
            )
            return
        self.stats_preview_windows.plot_bar_stats(stats)
        self.stats_preview_windows.show()




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    windows = MainWindows()
    app.exec_()
