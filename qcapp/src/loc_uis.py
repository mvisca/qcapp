import os 

def get_path_to(file, current=""):
    path = os.path.dirname(os.path.abspath(current))
    path = os.path.dirname(path)+ f"/ui/{file}"
    return path

def get_path_cfg(file, current=""):
    path = os.path.dirname(os.path.abspath(current))
    path = os.path.dirname(path)+ f"/config/{file}"
    return path
