from setuptools import setup

setup(
    # Application name:
    name="QcApp",

    # Version number (initial):
    version="0.0.1dev",

    # Application author details:
    author="Mario A. Visca",
    author_email="mvisca89@gmail.com",

    # Packages
    packages=["qcapp", "qcapp/src"],
    files_data={
    	"":["*.txt"]
    },

    # Include additional files into the package
    include_package_data=True,

    #
    # license="LICENSE.txt",
    description="",
    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    scripts=["bin/QcApp.py"],
    install_requires=open("requirements.txt").read(),
    python_requires='>=3.6',
	classifiers=[
    	'License :: OSI Approved :: MIT License',
    	'Programming Language :: Python :: 3.6',
	],
)
