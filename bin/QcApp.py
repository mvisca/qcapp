#!/bin/env python3
import os
import sys
import qcapp
path = os.path.dirname(os.path.abspath(qcapp.__file__))
sys.path.append(path+"/src")

from qcapp import main

print("Starting QcApp")
try:
	main.run()
except:
	pass
print("Closing QcApp")


